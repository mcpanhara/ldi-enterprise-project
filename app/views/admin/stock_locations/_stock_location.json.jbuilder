json.extract! stock_location, :id, :stock_name, :created_at, :updated_at
json.url stock_location_url(stock_location, format: :json)
