class HomesController < ApplicationController
  before_action :find_cart
  def index
    @products = Product.all.limit(10)
  end
  private
    def find_cart
      if current_customer
        @cart = session[:cart_id] ? Order.find(session[:cart_id]) : current_customer.orders.build
      end
    end
end
