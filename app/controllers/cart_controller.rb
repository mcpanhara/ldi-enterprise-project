class CartController < ApplicationController
  before_action :login_required
  before_action :find_cart

  # Add To Cart Method
  def add
    @cart.save if @cart.new_record?
    session[:cart_id] = @cart.id
    product = Product.find(params[:id])
    pro = product.quantity - 1
    product.update_attribute(:quantity, pro )
    LineItem.create! :order => @cart, :product => product, :sub_total => product.price
    @cart.recalculate_price!
    redirect_to products_path
  end

  #Remove Cart
  def remove
    item = @cart.line_items.find(params[:id])
    item.destroy
    product = Product.find(item['product_id'])
    pro = product.quantity + 1
    product.update_attribute(:quantity, pro)
    @cart.recalculate_price!
    redirect_to cart_path
  end

  #Checkout Product
  def checkout
    @cart.checkout!
    session.delete(:cart_id)
    respond_to do |format|
      format.html { redirect_to cart_path , notice: "Thank For Buying Our Products"}
    end
  end


    private
      def find_cart
        @cart = session[:cart_id] ? Order.find(session[:cart_id]) : current_customer.orders.build
      end
end
