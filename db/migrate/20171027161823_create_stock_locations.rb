class CreateStockLocations < ActiveRecord::Migration[5.1]
  def change
    create_table :stock_locations do |t|
      t.string :stock_name

      t.timestamps
    end
  end
end
