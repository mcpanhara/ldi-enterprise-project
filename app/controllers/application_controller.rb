class ApplicationController < ActionController::Base
  layout 'application'
  include Authentication
  protect_from_forgery with: :exception
  skip_before_action :verify_authenticity_token

  def after_sign_out_path_for(resource_or_scope)
    new_admin_user_session_path
  end

  def after_sign_in_path_for(resource_or_scope)
    admin_dashboard_path
  end

end
