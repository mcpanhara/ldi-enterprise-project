json.extract! stock_item, :id, :product_id, :stock_location_id, :quantity, :created_at, :updated_at
json.url stock_item_url(stock_item, format: :json)
