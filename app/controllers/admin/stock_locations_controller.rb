class Admin::StockLocationsController < AdminApplicationController
  before_action :set_stock_location, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_admin_user!
  before_action :admin_only, except: [:index,:show]
  # GET /stock_locations
  # GET /stock_locations.json
  def index
    @stock_locations = StockLocation.all
  end

  # GET /stock_locations/1
  # GET /stock_locations/1.json
  def show
  end

  # GET /stock_locations/new
  def new
    @stock_location = current_admin_user.stock_locations.build
  end

  # GET /stock_locations/1/edit
  def edit
  end

  # POST /stock_locations
  # POST /stock_locations.json
  def create
    @stock_location = current_admin_user.stock_locations.build(stock_location_params)

    respond_to do |format|
      if @stock_location.save
        format.html { redirect_to admin_stock_location_path(@stock_location), notice: 'Stock location was successfully created.' }
        format.json { render :show, status: :created, location: @stock_location }
      else
        format.html { render :new }
        format.json { render json: @stock_location.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /stock_locations/1
  # PATCH/PUT /stock_locations/1.json
  def update
    respond_to do |format|
      if @stock_location.update(stock_location_params)
        format.html { redirect_to admin_stock_location_path(@stock_location), notice: 'Stock location was successfully updated.' }
        format.json { render :show, status: :ok, location: @stock_location }
      else
        format.html { render :edit }
        format.json { render json: @stock_location.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /stock_locations/1
  # DELETE /stock_locations/1.json
  def destroy
    @stock_location.destroy
    respond_to do |format|
      format.html { redirect_to admin_stock_location_path(@stock_location), notice: 'Stock location was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_stock_location
      @stock_location = StockLocation.find(params[:id])
    end

    def admin_only
      unless current_admin_user.admin?
        redirect_to admin_dashboard_path, :notice => "Access denied."
      end
    end
    # Never trust parameters from the scary internet, only allow the white list through.
    def stock_location_params
      params.require(:stock_location).permit(:stock_name)
    end
end
