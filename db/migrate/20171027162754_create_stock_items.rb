class CreateStockItems < ActiveRecord::Migration[5.1]
  def change
    create_table :stock_items do |t|
      t.integer :product_id
      t.integer :stock_location_id
      t.integer :quantity, default: 0

      t.timestamps
    end
  end
end
