class Admin::StockItemsController < AdminApplicationController
  before_action :set_stock_item, only: [:index,:stock,:new,:create,:edit,:update,:destroy]
  before_action :authenticate_admin_user! , only: [:index,:new,:edit,:update,:destroy,:set_stock_item,:stock]
  before_action :admin_only, except: [:index,:show]

  # GET /stock_items
  # GET /stock_items.json

  def stock
    @stock_item = current_admin_user.stock_items.build
  end

  def index
    @stock_items = @product.stock_items.joins(:product)
    @fields = ["Product","Quantity", "Stock Location"]
  end

  # GET /stock_items/1
  # GET /stock_items/1.json
  def show

  end

  # GET /stock_items/new
  def new
  end

  # GET /stock_items/1/edit
  def edit
  end

  # POST /stock_items
  # POST /stock_items.json
  def create
    @product.save
    @stock = @product.stock_items.build(stock_item_params)
    @stock.user = current_admin_user
    respond_to do |format|
      if @stock.save
        format.html{ redirect_to admin_product_stock_path, notice: "Stock Has Been Added" }
      else
        format.html{ render :stock }
      end
    end
    # respond_to do |format|
    #   if @stock_item.save
    #
    #       redirect_to admin_product_stock_path,
    #       flash[:success] = 'Stock item was successfully created.'
    #     format.json { render :show, status: :created, location: @stock_item }
    #   else
    #     format.html { render :stock }
    #     format.json { render json: @stock_item.errors, status: :unprocessable_entity }
    #   end
    # end
  end

  # PATCH/PUT /stock_items/1
  # PATCH/PUT /stock_items/1.json
  def update
    respond_to do |format|
      if @stock_item.update(stock_item_params)
        format.html { redirect_to admin_product_stock_items, notice: 'Stock item was successfully updated.' }
        format.json { render :show, status: :ok, location: @stock_item }
      else
        format.html { render :edit }
        format.json { render json: @stock_item.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /stock_items/1
  # DELETE /stock_items/1.json
  def destroy
    @stock_item.destroy
    respond_to do |format|
      format.html { redirect_to admin_stock_items_url, notice: 'Stock item was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_stock_item
      @product = current_admin_user.products.find(params[:product_id])
    end

    def admin_only
      unless current_admin_user.admin?
        redirect_to admin_dashboard_path, :notice => "Access denied."
      end
    end
    # Never trust parameters from the scary internet, only allow the white list through.
    def stock_item_params
      params.require(:stock_item).permit(:product_id, :stock_location_id, :count_on_hand,:quantity)
    end
end
