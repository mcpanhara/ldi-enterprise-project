class Admin::UsersController < AdminApplicationController
  before_action :authenticate_admin_user!
  before_action :admin_only_and_vip_only, except: [:index,:show]

  def index
    @users = User.all
    @fields = ["ID","Username","Email","Phone","Address","Date Of Birth","Role"]
  end

  def show

    @user = User.find(params[:id])
    unless current_admin_user.admin?
      unless @user == current_admin_user
        redirect_to admin_root_path, :alert => "Access denied."
      end
    end
  end

  def new
    @user = User.new
  end

  def create
    @user = User.new(secure_params)
    if @user.save
      redirect_to admin_users_path(@user)
    else
      render :new
    end
  end

  def edit
    @user = User.find(params[:id])
  end


  def update
    @user = User.find(params[:id])
    no_requried_password
    if @user.update_attributes(user_params)
      redirect_to admin_user_path(@user), :notice => "User was successfully updated."
    else
      redirect_to admin_user_path(@user), :alert => "Unable to update user."
    end
  end

  def destroy
    user = User.find(params[:id])
    user.destroy
    redirect_to admin_users_path(user), :notice => "User was successfully deleted"
  end

  private

    def admin_only_and_vip_only
      unless current_admin_user.admin? || current_admin_user.vip?
        redirect_to admin_dashboard_path, :notice => "Access denied."
      end
    end

    def no_requried_password
      if params[:user][:password].blank?
        params[:user].delete("password")
        params[:user].delete("password_confirmation")
      end
    end

    # def vip_only
    #   unless current_user.vip?
    #     redirect_to dashboard_path
    #   end
    # end

    def secure_params
      params.require(:user).permit(:username,:email,:password,:password_confirmation,
                                    :role,:description,:image,:position,:dateofbirth,
                                    :address,:phone)
    end
    def user_params
      params.require(:user).permit(:username,:email,:password,
                                    :role,:description,:image,:position,:dateofbirth,
                                    :address,:phone)
    end
end
