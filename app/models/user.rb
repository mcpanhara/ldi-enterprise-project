class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable


  enum role: [:user, :vip, :admin]
  after_initialize :set_default_role, :if => :new_record?
  after_initialize :set_role_vip, :if => :new_record?

  has_many :categories
  has_many :products
  has_many :stock_locations
  has_many :stock_items
  
  #instance method
  def set_default_role
    self.role ||= :user
  end

  def set_role_vip
    self.role ||= :vip
  end
end
