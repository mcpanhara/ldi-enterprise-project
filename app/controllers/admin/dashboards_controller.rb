class Admin::DashboardsController < AdminApplicationController
  before_action :authenticate_admin_user!
  before_action :admin_only, :except => :index
  def index

  end

  private
  def admin_only
    unless current_admin_user.admin?
      redirect_to admin_dashboard_path, :notice => "Access denied."
    end
  end
end
