class ::Admin::Users::SessionsController < Devise::SessionsController
  layout 'admin_application'
  # before_action :configure_sign_in_params, only: [:create]
  after_action :message_sign_out, only: :destroy
  after_action :message_sign_in, only: [:create,:new]
  # GET /resource/sign_in
  def new
    super
    flash[:message]
  end

  # POST /resource/sign_in
  def create
    super
    flash[:message]
  end

  # DELETE /resource/sign_out
  def destroy
    super
    flash[:message]
  end

  # protected

  # If you have extra params to permit, append them to the sanitizer.
  # def configure_sign_in_params
  #   devise_parameter_sanitizer.permit(:sign_in, keys: [:attribute])
  # end
  private
    def message_sign_out
      flash[:message] = flash[:notice]
      flash[:message]
    end

    def message_sign_in
      flash[:message] = flash[:notice]
      flash[:message]
    end
end
