class SessionsController < ApplicationController
  before_action :find_cart, only: [:destroy]
  def new
  end

  def create
    customer = Customer.authenticate(params[:login], params[:password])

    if customer
      session[:customer_id] = customer.id
      flash[:notice] = "Thank For Login To Our Website"
      redirect_to products_path
    else
      flash[:notice] = "Invalid Account or Password"
      redirect_to login_path
    end

  end

  def destroy
    session[:customer_id] = nil
    # WIP
    session.delete(:cart_id)

    flash[:notice] = "You account is logged out"
    redirect_to login_path
  end

  protected
    def find_cart
      @cart = session[:cart_id] ? Order.find(session[:cart_id]) : current_customer.orders.build
    end
end
