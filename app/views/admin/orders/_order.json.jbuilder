json.extract! order, :id, :customer_id, :checked_out_at, :total_price, :created_at, :updated_at
json.url order_url(order, format: :json)
