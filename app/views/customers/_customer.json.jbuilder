json.extract! customer, :id, :username, :email, :password_hashL, :password_salt, :image, :created_at, :updated_at
json.url customer_url(customer, format: :json)
