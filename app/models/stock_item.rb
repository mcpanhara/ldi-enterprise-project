class StockItem < ApplicationRecord
  belongs_to :user
  belongs_to :stock_location
  belongs_to :product
end
