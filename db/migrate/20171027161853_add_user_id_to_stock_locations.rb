class AddUserIdToStockLocations < ActiveRecord::Migration[5.1]
  def change
    add_column :stock_locations, :user_id, :integer
  end
end
